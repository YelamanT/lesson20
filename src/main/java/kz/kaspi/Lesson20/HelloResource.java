package kz.kaspi.Lesson20;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;
import kz.kaspi.Lesson20.Dao.EmployeeDao;
import kz.kaspi.Lesson20.entity.Employee;

import java.util.List;

@Path("/employee-database")
public class HelloResource {

    EmployeeDao service = new EmployeeDao();


    @POST
    @Produces ("application/json")
    @Consumes ("application/json")
    @Path("/new")
    public Response newEmployee(Employee emp) {
        if (emp.getAge()<=0 || emp.getName().isEmpty() || emp.getDepartment().isEmpty()) {
            return Response.status(400, "Неверные параметры сотрудника").build();
        }
        service.addEmployee(emp);

        return Response.ok(emp).build();
    }


    @GET
    @Produces("application/json")
    public List<Employee> getAllEmployees () {
        List<Employee> list =service.getAll();
        return list;
    }

    @GET
    @Produces("application/json")
    @Path("/{id}")
    public Response find(@PathParam (value="id") Long id) {
        if (id==null) {
            return Response.status(400, "Id не может быть пустым").build();
        }
        Employee emp = new Employee();
        emp=service.findById(id);
        return Response.ok(emp).build();
    }

    @DELETE
    @Produces ("application/json")
    @Path("/delete-{id}")
    public Response deleteEmployee (@PathParam(value= "id") Long id) {
        if (id==null) {
            return Response.status(400, "Id не может быть пустым").build();
        }
        Employee emp = new Employee();
        emp= service.findById(id);
        service.deleteEmployee(emp);

        return Response.ok(emp).build();
    }

    @PUT
    @Produces("application/json")
    @Consumes ("application/json")
    @Path("/update")
    public Response updateEmployee (Employee emp) {
        if (emp.getId()==null) {
            return Response.status(400, "Id не может быть пустым").build();
        }

        service.updateEmployee(emp);
        return Response.ok(emp).build();
    }
}