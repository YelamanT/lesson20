package kz.kaspi.Lesson20.Dao;

import kz.kaspi.Lesson20.entity.Employee;
import kz.kaspi.Lesson20.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDao {
    private Session currentSession;
    private Transaction currentTransaction;

    public List<Employee> getAll () {
        List <Employee> list = new ArrayList<>();
        currentSession= HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();

        list=currentSession.createQuery("FROM Employee").list();

        currentTransaction.commit();
        currentSession.close();

        return list;
    }

    public void addEmployee (Employee emp) {
        currentSession= HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();

        currentSession.persist(emp);

        currentTransaction.commit();
        currentSession.close();
    }

    public Employee findById (Long id) {
        currentSession= HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();

        Employee emp = currentSession.get(Employee.class,id);

        currentTransaction.commit();
        currentSession.close();

        return emp;
    }

    public void updateEmployee (Employee emp) {
        currentSession= HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();

        currentSession.update(emp);

        currentTransaction.commit();
        currentSession.close();
    }

    public void deleteEmployee (Employee emp) {
        currentSession= HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();

        currentSession.delete(emp);

        currentTransaction.commit();
        currentSession.close();
    }


}
